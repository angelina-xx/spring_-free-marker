<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>index</title>
    <link rel="stylesheet" type="text/css" href="../static/index.css" >
</head>
<body>
<#--    <H1>尊敬的 ${name} ${sex}:</H1>-->
    <H1 class="name">尊敬的小谢的朋友们:</H1>
    <h2 class="content">${content}</h2>
    <h3>地点：${address}<br/>时间：${time?string("yyyy年MM月dd日 HH:mm")}</h3>
    <img src="./static/test.png" />

</body>
</html>