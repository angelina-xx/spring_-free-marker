package com.yc.Quartz;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 定时器任务
 */
//@Component
public class SchedulerTask {


    public void Scheduler() {
        try {
            // 首先，调用StdSchedulerFactory.getDefaultScheduler()方法创建一个调度器对象scheduler
            // 创建调度器
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

            // 然后，使用TriggerBuilder.newTrigger()创建一个触发器对象trigger，并使用withIdentity()方法设置唯一标识符，
            // 通过withSchedule()方法设置触发的时间规则，这里使用了Cron表达式"0 0/1 * * * ?"，表示每分钟执行一次
            // Cron表达式格式：{秒数} {分钟} {小时} {日期} {月份} {星期} {年份(可为空)}
            // 定义一个触发器，指定执行的时间规则

            Trigger trigger = TriggerBuilder.newTrigger()
                    .withIdentity("trigger2", "group2")
                    .withSchedule(CronScheduleBuilder.cronSchedule("01 41 10 12 8 ? 2023")) // 每秒钟执行一次
                    .build();

            JobDetail job = JobBuilder.newJob(MyJob.class)
                    .withIdentity("job2", "group2")
                    .build();

            // 最后，调用scheduler.scheduleJob()方法将触发器和任务绑定到调度器中，并调用scheduler.start()方法启动调度器，开始定时执行任务
            // 将触发器和任务绑定到调度器中
            scheduler.scheduleJob(job, trigger);
            System.out.println("--------scheduler start ! ------------");
            // 启动调度器
            scheduler.start();

        } catch (SchedulerException e) {
            System.out.println("任务调度失败！");
            e.printStackTrace();
        }
    }

}
