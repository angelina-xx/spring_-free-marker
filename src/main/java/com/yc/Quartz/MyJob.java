package com.yc.Quartz;
import com.yc.bean.Invitation;
import com.yc.email.Email;
import com.yc.email.EmailService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 创建一个MyJob类，实现org.quartz.Job接口。
 * 在该类中定义要执行的任务逻辑。
 */
@Slf4j
//@Component
//@ConditionalOnBean(Invitation.class)
public class MyJob implements Job {

    @Autowired
    private EmailService emailService;

    //邀请函对象
    @Autowired
    private Invitation invitation;

    public void execute(JobExecutionContext context) throws JobExecutionException {



        // 编写要执行的任务逻辑
        System.out.println("定时任务执行了");

        List<String> receiverList = new ArrayList<>();
        receiverList.add("2105881412@qq.com");

        //邮件对象
        Email email = new Email();
        //邮件发送方
        email.setSender("1804113856@qq.com");
        //邮件收件人
        email.setReceiverList(receiverList);
        //邮件主题
        email.setSubject("邀请函");

        //设置模板数据
        Map<String,Object> data = new HashMap<>();
        data.put("name",invitation.getName());
        data.put("sex",invitation.getSex());
        data.put("content",invitation.getContent());
        data.put("address",invitation.getAddress());
        data.put("time",invitation.getTime());
        boolean b = emailService.sendSimpleEmail(email,"index.ftl",data);
        log.info("发送结果:{}",b);

        // 任务执行完成后，关闭Scheduler调度器
        Scheduler scheduler = context.getScheduler();
        try {
            scheduler.shutdown();
            System.out.println("--------scheduler shutdown ! ------------");

        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }
}