package com.yc.bean;

import com.yc.bean.Invitation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Controller //不能使用ReController，因为ReController包括了ReposeBody @RestController注解表示返回的内容都是HTTP Content不会被模版引擎处理的
@RequestMapping("/index")

public class MyController {

    //邀请函对象
    @Autowired
    private Invitation invitation;

    @GetMapping
    public String index(Model model){ //创建模板对象
        //模板添加属性
        model.addAttribute("name",invitation.getName());
        model.addAttribute("sex",invitation.getSex());
        model.addAttribute("content",invitation.getContent());
        model.addAttribute("address",invitation.getAddress());
        model.addAttribute("time",invitation.getTime());
        return "index";
    }
}
