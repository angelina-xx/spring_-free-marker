package com.yc.bean;

import lombok.Data;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@Data
@ToString
@ConfigurationProperties(prefix = "invitation")
@Primary
public class Invitation {

    private String name; //姓名
    private String sex; //女生/先生
    private String content; //邀请函内容l
    private String address; //地址
    private Date time; //时间

}
