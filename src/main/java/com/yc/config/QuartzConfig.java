package com.yc.config;

import com.yc.bean.Invitation;
import com.yc.email.Email;
import com.yc.email.EmailService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//@Configuration
@Slf4j
public class QuartzConfig {

    @Autowired
    private EmailService emailService;

    //邀请函对象
    @Autowired
    private Invitation invitation;

    // 任务实现类
    public class Task1 extends QuartzJobBean {
        @Override
        protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
            // 编写要执行的任务逻辑
            System.out.println("定时任务执行了");

            List<String> receiverList = new ArrayList<>();
            receiverList.add("2105881412@qq.com");
//            receiverList.add("2774834896@qq.com");
//            receiverList.add("2105881412@qq.com");
//            receiverList.add("2073028913@qq.com");

            //邮件对象
            Email email = new Email();
            //邮件发送方
            email.setSender("1804113856@qq.com");
            //邮件收件人
            email.setReceiverList(receiverList);
            //邮件主题
            email.setSubject("邀请函");

            //设置模板数据
            Map<String,Object> data = new HashMap<>();
            data.put("name",invitation.getName());
            data.put("sex",invitation.getSex());
            data.put("content",invitation.getContent());
            data.put("address",invitation.getAddress());
            data.put("time",invitation.getTime());
            boolean b = emailService.sendSimpleEmail(email,"index.ftl",data);
            log.info("发送结果:{}",b);

            // 任务执行完成后，关闭Scheduler调度器
            Scheduler scheduler = context.getScheduler();
            try {
                scheduler.shutdown();
                System.out.println("--------scheduler shutdown ! ------------");

            } catch (SchedulerException e) {
                e.printStackTrace();
            }
        }
    }

    // 任务明细
    @Bean
    public JobDetail jobDetail(){
        return JobBuilder.newJob(Task1.class).storeDurably().build();
    }

    // 绑定任务触发器
    @Bean
    public Trigger printJobTrigger(@Autowired JobDetail jobDetail){
        // cron表达式 0 0/1 * * * ?
        return TriggerBuilder.newTrigger().forJob(jobDetail)
                .withSchedule(CronScheduleBuilder.cronSchedule("01 50 11 15 8 ? 2023")).build();
    }
}
