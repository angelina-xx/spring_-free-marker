import com.yc.ApplicationRun;
import com.yc.bean.Invitation;
import com.yc.email.Email;
import com.yc.email.EmailService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {ApplicationRun.class})
@Slf4j
public class SendMailTest {

    //自动注入FreeMarker配置类 用户获取模板
    @Autowired
    private EmailService emailService;

    //邀请函对象
    @Autowired
    private Invitation invitation;

    @Test
    public void test(){
        List<String> receiverList = new ArrayList<>();
        receiverList.add("2105881412@qq.com");

        //邮件对象
        Email email = new Email();
        //邮件发送方
        email.setSender("1804113856@qq.com");
        //邮件收件人
        email.setReceiverList(receiverList);
        //邮件主题
        email.setSubject("邀请函");

        //设置模板数据
        Map<String,Object> data = new HashMap<>();
        data.put("name",invitation.getName());
        data.put("sex",invitation.getSex());
        data.put("content",invitation.getContent());
        data.put("address",invitation.getAddress());
        data.put("time",invitation.getTime());
        boolean b = emailService.sendSimpleEmail(email,"index.ftl",data);
        log.info("发送结果:{}",b);
    }

}
